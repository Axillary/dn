﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace MissionPlanner
{
	class MyWebRequest
	{
		private HttpWebRequest request;
		private Stream dataStream;
		static private CookieContainer cookie_container;
		public Cookie cookie;

		private string status;

		public String Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}



		public MyWebRequest(string url)
		{
			// Create a request using a URL that can receive a post.

			request = (HttpWebRequest)WebRequest.CreateHttp(url);
            request.Timeout = 8000;
			if (cookie_container == null)
			{
				cookie_container = new CookieContainer();
			}

			request.CookieContainer = cookie_container;



		}

		public MyWebRequest(string url, string method)
			: this(url)
		{

			if (method.Equals("GET") || method.Equals("POST") || method.Equals("PUT") || method.Equals("DELETE"))
			{
				// Set the Method property of the request to POST.
				request.Method = method;
			}
			else
			{
				throw new Exception("Invalid Method Type");
			}
		}

		public MyWebRequest(string url, string method, string data)
			: this(url, method)


		{


			// Create POST data and convert it to a byte array.
			string postData = data;
			byte[] byteArray = Encoding.UTF8.GetBytes(postData);


			// Set the ContentType property of the WebRequest.


			request.ContentType = "application/x-www-form-urlencoded";


			// Set the ContentLength property of the WebRequest.
			request.ContentLength = byteArray.Length;

			// Get the request stream.
			dataStream = request.GetRequestStream();

			// Write the data to the request stream.
			dataStream.Write(byteArray, 0, byteArray.Length);

			// Close the Stream object.
			dataStream.Close();



		}

		public MyWebRequest(string url, string method, byte[] byteArray)
			: this(url, method)


		{
			// Set the ContentType property of the WebRequest.

			if (url.EndsWith("odlcs") == true || url.EndsWith("obstacles") == true|| url.EndsWith("telemetry") == true)
			{
				request.ContentType = "application/json";
			}

			if (url.EndsWith("image") == true)
			{
				request.ContentType = "image/jpg";
			}
			else
			{
				request.ContentType = "application/json";
			}


			// Set the ContentLength property of the WebRequest.
			request.ContentLength = byteArray.Length;

			// Get the request stream.
			dataStream = request.GetRequestStream();

			// Write the data to the request stream.
			dataStream.Write(byteArray, 0, byteArray.Length);

			// Close the Stream object.
			dataStream.Close();



		}


		public string GetResponse()
		{
			// Get the original response.
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();

			this.Status = ((HttpWebResponse)response).StatusDescription;

			// Get the stream containing all content returned by the requested server.
			dataStream = response.GetResponseStream();


			// Open the stream using a StreamReader for easy access.
			StreamReader reader = new StreamReader(dataStream);

			// Read the content fully up to the end.
			string responseFromServer = reader.ReadToEnd();

			// Clean up the streams.
			foreach (Cookie cook in response.Cookies)
			{
				cookie = cook;
			}

			return responseFromServer ;
		}

	}
}
