﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionPlanner
{
    class Waypoints
    {
        public double lat;
        public double lon;
        public double alt;
        public int Radpass;
        public int ord;
        public int CurrentPass;

        public Waypoints()
        {
            Radpass = 0;
            CurrentPass = 0;
        }
    }
}
